from setuptools import setup
#from distutils.core import setup
import os

def get_data_files():
	ret = []
	#for root, dirs, files in os.walk("data"):
	#	for file in files:
	#		tmp = root + "/" + file
	#		ret.append(tmp)
	tmp = os.listdir("data")
	for file in tmp:
		if file != "glade":
			ret.append("data/" + file)
	return ret

def get_glade_files():
	ret = []
	tmp = os.listdir("data/glade")
	for file in tmp:
		ret.append("data/glade/" + file)
	return ret



if __name__ == "__main__":
	
	setup (
		name = 'cGmail',
		version = '0.5',
		description = 'A new shiny email checker for the gnome desktop',
		author = 'Marco Ferragina',
		author_email = 'marco.ferragina@gmail.com',
		url = 'http://cgmail.tuxfamily.org',
		license = 'GPL',
		scripts = ['cgmail', 'cgmailservice'],
		packages = [
			'cGmail', 
			'cGmail.checkers',
			'cGmail.lib',
			'cGmail.manager',
			'cGmail.service'
		],
		data_files = [
			('share/cGmail', get_data_files()), 
			('share/cGmail/glade', get_glade_files()), 
			('share/pixmaps', ['data/cgmail.svg']),
		]
	)

