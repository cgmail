#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import gtk
import thread
import time
import traceback

if gtk.pygtk_version < (2, 10, 0):
	import sys
	print _("You need pygtk >= 2.10 to run cgmail.")
	sys.exit(1)

import dbus
import dbus.mainloop.glib
import gobject

import checkersrunner
from statusicon import StatusIcon
from statusnotifier import StatusNotifier
from lib.gconfhelper import GconfHelper
from dbusinterface import ServiceDbusInterface


#gobject.threads_init()
gtk.gdk.threads_init()

class MainLoop:
	def __init__(self):
		self.crunner = checkersrunner.CheckersRunner()
		self.crunner.init_checkers()

		StatusIcon(self.crunner)
		StatusNotifier(self.crunner)
		self.gconf_helper = GconfHelper()

		# start dbus service
		dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
		session_bus = dbus.SessionBus()
		name = dbus.service.BusName("org.tuxfamily.cgmail.Service", session_bus)
		dbus_service = ServiceDbusInterface(session_bus, '/Service')
		dbus_service.set_on_exit_cb(self.stop)
		dbus_service.set_on_refresh_cb(self.force_check)

		try:
			gobject.idle_add(self.main_iteration)
			gtk.main()
		except KeyboardInterrupt:
			pass
	
	def force_check(self):
		print "force check"
		self.crunner.reset()
		try:
			self.crunner.check()
		except Exception, e:
			traceback.print_exc()
			print "Warning: ", e

	def stop(self):
		print "stop called"
		gtk.main_quit()

	def main_iteration(self):
		sleep_time = self.gconf_helper.get_key("check_interval")
		try:
			self.crunner.check()
		except Exception, e:
			traceback.print_exc()
			print "Warning: ", e
		gobject.timeout_add(sleep_time * 1000, self.main_iteration)

		# Don't return True here. See gobject.timeout_add docs
		return False

