#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

from statuslistener import StatusListener

from lib.gconfhelper import GconfHelper
from lib import utils
from lib.common import *
from lib.notifier import Notifier


class StatusNotifier(StatusListener):
	def __init__(self, crunner):
		StatusListener.__init__(self, crunner)
	
	def on_status_changed(self, crunner, checker):
		gtk.gdk.threads_enter()

		notifier = Notifier()
		status = checker.get_status()
		if status["count"] > 0 \
			and status["description"].strip() != "" and status["title"].strip() != "":

			mustnotify = GconfHelper().get_key("display_notifications")
			if mustnotify:
				notifier.set_actions(checker.get_notifier_actions())
				notifier.notify(status["title"], 
						status["description"],
						msec = 10000)
			mustplaysound = GconfHelper().get_key("play_sounds_on_new_mails")
			if mustplaysound:
				utils.playsnd(SND_NOTIFY)

			runcommand = GconfHelper().get_key("exec_command")
			if runcommand:
				cmd = GconfHelper().get_key("new_mail_command")
				cmdline = cmd.split()
				utils.invoke_subprocess(cmdline)

		gtk.gdk.threads_leave()




