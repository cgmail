#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import dbus
import dbus.service
import dbus.mainloop.glib

class ServiceDbusInterface(dbus.service.Object):
	def __init__(self, bus, obj):
		dbus.service.Object.__init__(self, bus, obj)

		self.on_exit_cb = None
		self.on_refresh_cb = None
	
	@dbus.service.method("org.tuxfamily.cgmail.ServiceInterface",
				in_signature="", out_signature="")
	def exit(self):
		print "exit called"
		if self.on_exit_cb is not None:
			self.on_exit_cb()
	
	@dbus.service.method("org.tuxfamily.cgmail.ServiceInterface",
				in_signature="", out_signature="")
	def refresh(self):
		print "refresh called"
		if self.on_refresh_cb is not None:
			self.on_refresh_cb()
	
	def set_on_refresh_cb(self, cb):
		self.on_refresh_cb = cb
	
	def set_on_exit_cb(self, cb):
		self.on_exit_cb = cb
	

def get_dbus_interface():
	dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
	bus = dbus.SessionBus()
	try:
		obj = bus.get_object("org.tuxfamily.cgmail.Service", "/Service")
		iface = dbus.Interface(obj, "org.tuxfamily.cgmail.ServiceInterface")
	except dbus.DBusException:
		return None

	return iface

