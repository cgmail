#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import time
import thread
import gobject

from lib.common import *
from lib.accountmanager import AccountManager
from lib.checkersutils import load_checkers
from lib.gconfhelper import GconfHelper
from lib.notifier import Notifier

class CheckersRunner(gobject.GObject):
	__gsignals__ = {
		'checker-status-changed' : (
			gobject.SIGNAL_RUN_LAST,
			gobject.TYPE_NONE,
			(gobject.TYPE_PYOBJECT,) # The checker with changed status
		)
	}

	def __init__(self):
		gobject.GObject.__init__(self)

		self.checkers = {} # account_id: checker
		self.checking = False
	
	def init_checkers(self):
		"""
		Build checkers list.
		"""
		am = AccountManager()
		accounts = am.get_accounts_dicts()
		if accounts is None or len(accounts) == 0:
			self.checkers = {}
			# nothing more to do
			return

		id_list = []
		for account in accounts:
			id_list.append(account["id"])

			needed_keys = ["type", "username", "password", "enabled"]
			has_needed = True
			for k in needed_keys:
				if not account.has_key(k):
					print "Warnig: bad configuration"
					has_needed = False
					break

			if not has_needed: continue

			if account["enabled"] == "0":
				# we no more want this checker
				if self.checkers.has_key(account["id"]):
					checker = self.checkers[account["id"]]
					# force emit status-changed signal to update
					# staus listners
					checker.emit("status-changed", 0, "", "", None)
					del self.checkers[account["id"]]
				continue

			if account["id"] in self.checkers.keys():
				# We already have a checker for this account
				checker = self.checkers[account["id"]]
				checker.update_info(account)
				continue
			modules = load_checkers()
			for mod in modules:
				tmp = mod.Checker(account)
				if tmp.inited():
					tmp.connect("status-changed", 
						self.on_checker_status_changed)
					tmp.connect("checking-error", self.on_checking_error)
					self.checkers[account["id"]] = tmp
		
		# remove checker if the account no more exist
		for id in self.checkers.keys():
			if id not in id_list:
				del self.checkers[id]

	def on_checking_error(self, checker, title, text, is_critical):

		notify_errors = GconfHelper().get_key("notify_errors")
		if not is_critical:
			if not notify_errors:
				return

		Notifier().notify(title, text, msec = 10000)

	
	def on_checker_status_changed(self, checker_obj, *args):
		""" 
		Here we emit a runner-status-changed signal. This signal will
		be manged by status listners as status icon
		"""
		self.emit("checker-status-changed", checker_obj)

	def reset(self):
		for account_id, checker in self.checkers.iteritems():
			checker.reset()
		#for cb in self.status_cbs:
		#	cb(0, None, None)

	def check(self):
		self.init_checkers()
		mailslists = []
		#for account_id, values in self.checkers.iteritems():
		for account_id, checker in self.checkers.iteritems():
			thread.start_new_thread(checker.check, ())
	
if __name__ == "__main__":
	c = Checker()
	c.check()
