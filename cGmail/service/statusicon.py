#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import gtk
import gtk.glade
import gobject

import os

from lib.common import *
from lib.gconfhelper import GconfHelper
from lib import utils
from lib.utils import invoke_subprocess
from statuslistener import StatusListener
from lib.notifier import Notifier

GLADE_FILE = os.path.join(GLADE_BASE_PATH, "statusicon_popupmenu.glade")

class StatusIcon(StatusListener):
	def __init__(self, crunner):
		StatusListener.__init__(self, crunner)
		
		#self.crunner.add_status_cb(self.set_status)
		gtk.window_set_default_icon_from_file(NOMAIL_ICON)

		self.widgets = gtk.glade.XML(GLADE_FILE, domain="cgmail")
		self.menu = self.widgets.get_widget("menu")
		dic = {
			"on_configure_activate" : self.on_configure_activate,
			"on_reshow_last_notification_activate" : self.on_reshow_last_notification
		}

		self.widgets.signal_autoconnect(dic)
	
		self.status_icon = gtk.StatusIcon()
		#self.status_icon.set_from_stock(gtk.STOCK_CLOSE)
		self.status_icon.set_from_file(NOMAIL_ICON)
		self.status_icon.connect("popup-menu", self.on_popup_menu)
		self.status_icon.connect("activate", self.on_activate)

		self.call_manage_accounts = False
				
		self.gconf_helper = GconfHelper()
		self.gconf_helper.register_key_listner("always_show_status_icon", 
						self.status_icon_key_listner)
		

		self.checkers_count = {} # checker_id : msgs_count
		self.hide()
		
	def status_icon_key_listner(self, client, *args):
		"""
		Listen for gconf always_show_status_icon key changes
		"""
		if self.gconf_helper.get_key("always_show_status_icon"):
			self.status_icon.set_visible(True)
		else:
			total = 0
			for id, (count, checker_obj) in self.checkers_count.iteritems():
				total += count
			if total == 0:
				self.status_icon.set_visible(False)

	
	def hide(self):
		if not self.gconf_helper.get_key("always_show_status_icon"):
			self.status_icon.set_visible(False)

	def show(self):
		self.status_icon.set_visible(True)

	def on_status_changed(self, crunner, checker):
		gtk.gdk.threads_enter()

		status = checker.get_status()
		id = checker.get_id()
		self.checkers_count[id] = (status["count"], checker)
		total = 0
		for id, (count, checker_obj) in self.checkers_count.iteritems():
			total += count
		if total == 0:
			self.hide()
			self.status_icon.set_from_file(NOMAIL_ICON)
		elif total > 5:
			self.status_icon.set_from_file(MORE_NEWMAIL_ICON)
			self.show()
		else:
			self.status_icon.set_from_file(NEWMAIL_ICON_BASE_PATH % total)
			self.show()

		gtk.gdk.threads_leave()

	def on_reshow_last_notification(self, arg):
		self.on_activate(None)

	def on_activate(self, arg):
		notifier = Notifier()
		for id, (count, checker_obj) in self.checkers_count.iteritems():
			if count > 0:
				status = checker_obj.get_status()
				notifier.set_actions(checker_obj.get_notifier_actions())
				notifier.notify(status["title"], 
						status["description"],
						msec = 10000)

	def on_configure_activate(self, arg):
		if not invoke_subprocess("cgmail"):
			invoke_subprocess("./cgmail")
	
	def on_popup_menu(self, si, button, time):
		#self.menu.popup(None, None, None, button, time, si)
		self.menu.popup(None, None, gtk.status_icon_position_menu,
				button, time, si)
	

