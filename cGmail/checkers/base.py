#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

from lib.common import *
from manager.dbusinterface import get_dbus_interface as get_manager_dbus_interface 

import gobject

class BaseInfo:
	
	def get_name(self):
		raise NotImplementedError()
	
	def get_icon(self):
		""" Return a default checker icon. Subclass should overraide this
		    method and return an appropriate icon"""
		return DEFAULT_NOTIFY_ICON 
	
	def get_notify_icon(self):
		pass
	
	def get_sensible_data_list(self):
		"""
		Must return a list of sensible data. Sensible data will be stored
		in gnome keyring. Other data will be stored in a more flexible way
		using plain text file. Example of sensible data:
			["password", "username"]
		"""
		raise NotImplementedError()
	
class BaseGui:
	""" This is the base class for checker configuration gui"""
	def __init__(self, account_manager, base_widg):
		self.amanager = account_manager
		self.__base_widget = base_widg
	
	def attach(self, widg):
		try:
			self.__base_widget.add_with_viewport(widg)
		except Exception:
			self.__base_widget.add(widg)
		self.__base_widget.show_all()

	
	def fill(self, info):
		raise NotImplementedError()

	def add_checker(self):
		raise NotImplementedError()		
	
	def update_checker(self, info):
		raise NotImplementedError()		


class BaseChecker(gobject.GObject):
	# http://www.sicem.biz/personal/lgs/docs/gobject-python/gobject-tutorial.html
	__gsignals__ = {
		'status-changed' : (
			gobject.SIGNAL_RUN_FIRST,	# Run first self.do_status_changed
			gobject.TYPE_NONE,
			(
				gobject.TYPE_INT, 	# count
				gobject.TYPE_STRING,	# title
				gobject.TYPE_STRING,	# message
				gobject.TYPE_STRING	# icon path
			)
		),
		'checking-error' : (
			gobject.SIGNAL_RUN_FIRST,
			gobject.TYPE_NONE,
			(
				gobject.TYPE_STRING,	# title
				gobject.TYPE_STRING,	# text
				gobject.TYPE_BOOLEAN	# is_critical
			)
		)

	}

	def __init__(self, account_id):
		gobject.GObject.__init__(self)

		self.__account_id = account_id

		self.checking = False
		self.manager_iface = None

		# Internal status. It's set to true by childs if they
		# are initialized correctly
		self.__inited = False

		# Dict of values rappresenting checker status
		self.__status = {}
	
	def do_status_changed (self, count, title, description, icon_path):
		"""
		Set the internal status when the status changed signal is emitted. The
		signal should be captured by the gnome background service to notify the
		user about status changes for this checker.
		Values description:
			count		: is the count of the new mails into mailbox.
			title		: a brief title. Example: There are 2 new mails
			description	: a description summary
		"""
		self.__status = {
			"count"		: count,
			"title"		: title,
			"description"	: description,
			"icon_path"	: icon_path
		}
	
	def account_deleted_cb(self, cb):
		"""
		This is called from account manager when an account is deleted.
		Override this method on your childs if you need extra clean up
		"""
		pass

	def get_name(self):
		"""
		Return a brief name. 
		Example for a mailbox:
			moon@sun.org
		"""
		raise NotImplementedError()
	
	def get_id(self):
		"""
		return the checker id
		"""
		return self.__account_id
	
	def get_status(self):
		"""
		Return checker status dic
		"""
		return self.__status
	
	def set_inited(self, value):
		self.__inited = value

	def inited(self):
		return self.__inited
		
	def reset(self): 
		"""
		After this method is invoked, the checker must consider
		all mails as not notified
		"""
		raise NotImplementedError()

	def __error(self, e): pass
	def __reply(self): pass
	
	def start_check(self):
		self.checking = True
		gtk.gdk.threads_enter()
		try:
			self.manager_iface = get_manager_dbus_interface()
			if self.manager_iface is not None:
				self.manager_iface.set_refreshing(self.__account_id, True,
							reply_handler = self.__reply,
							error_handler = self.__error)
		except:
			print "Warning: BaseChecker -> start_check"
			pass
		gtk.gdk.threads_leave()
		
	def stop_check(self):
		self.checking = False
		gtk.gdk.threads_enter()
		try:
			if self.manager_iface is not None:
				self.manager_iface.set_refreshing(self.__account_id, False,
							reply_handler = self.__reply,
							error_handler = self.__error)
		except:
			print "Warning: BaseChecker -> stop_check"
			pass
		gtk.gdk.threads_leave()
	
	def get_notifier_actions(self):
		"""
		Must return a dict with actname as keys and a tuple as values.
		Each tuple must have two elements:
			first:	acts handlers
			second: acts Description (this value will be the button label)
		Actions will be used by notifier to build actions
		buttons on notify bubbles
		"""
		raise NotImplementedError()
	
	def check(self): 
		raise NotImplementedError()
	
	def update_info(self, account):
		"""
		This method receive an account dic and update checker info
		using account values
		"""
		raise NotImplementedError()

