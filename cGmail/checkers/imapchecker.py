#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

from lib.imap import *
from base import *
from lib.common import *

from lib.common import ICONS_BASE_PATH
from lib import utils

class Info(BaseInfo):
	def get_name(self): 
		return "imap"

	def get_icon(self):
		return ICONS_BASE_PATH + "imap.png"
	
	def get_sensible_data_list(self):
		return ["username", "password"]


class Gui(BaseGui):
	def __init__(self, amanager, base_widg):
		BaseGui.__init__(self, amanager, base_widg)
		vbox = gtk.VBox()
		settings_frame = gtk.Frame( _("<b>Account Settings</b>") )
		label1 = settings_frame.get_label_widget()
		label1.set_use_markup(True)
		#settings_frame.set_border_width(8)
		settings_frame.set_shadow_type(gtk.SHADOW_NONE)
		settings_align = gtk.Alignment()
		settings_align.set_padding(0, 0, 12, 0)
		settings_frame.add(settings_align)
		
		conn_settings_frame = gtk.Frame( _("<b>Connection Settings</b>") )
		label2 = conn_settings_frame.get_label_widget()
		label2.set_use_markup(True)
		#conn_settings_frame.set_border_width(8)
		conn_settings_frame.set_shadow_type(gtk.SHADOW_NONE)
		conn_align = gtk.Alignment()
		conn_align.set_padding(0, 0, 12, 0)
		conn_settings_frame.add(conn_align)

		conn_type_frame = gtk.Frame( _("<b>Connection Type</b>") )
		label3 = conn_type_frame.get_label_widget()
		label3.set_use_markup(True)
		#conn_type_frame.set_border_width(8)
		conn_type_frame.set_shadow_type(gtk.SHADOW_NONE)
		type_align = gtk.Alignment()
		type_align.set_padding(20, 20, 20, 0)
		conn_type_frame.add(type_align)

		mailbox_frame = gtk.Frame( _("<b>IMAP Mailbox</b>") )
		label4 = mailbox_frame.get_label_widget()
		label4.set_use_markup(True)
		#mailbox_frame.set_border_width(8)
		mailbox_frame.set_shadow_type(gtk.SHADOW_NONE)
		mailbox_align = gtk.Alignment()
		mailbox_align.set_padding(20, 0, 20, 0)
		mailbox_frame.add(mailbox_align)

		vbox.pack_start(settings_frame, True, True, 0)
		vbox.pack_start(conn_settings_frame, True, True, 0)
		vbox.pack_start(conn_type_frame, True, True, 0)
		vbox.pack_start(mailbox_frame, True, True, 0)
		
		acc_table = gtk.Table(2, 2, True)
		settings_align.add(acc_table)
		
		username_label = gtk.Label( _("Username: ") )
		acc_table.attach(username_label, 0, 1, 0, 1, ypadding = 10)
		self.username_entry = gtk.Entry()
		acc_table.attach(self.username_entry, 1, 2, 0, 1, xpadding = 10)
		password_label = gtk.Label( _("Password: ") )
		acc_table.attach(password_label, 0, 1, 1, 2, ypadding = 10)
		self.password_entry = gtk.Entry()
		self.password_entry.set_visibility(False)
		acc_table.attach(self.password_entry, 1, 2, 1, 2, xpadding = 10)

		conn_table = gtk.Table(2, 2, True)
		conn_align.add(conn_table)
		
		server_label = gtk.Label( _("Server: ") )
		conn_table.attach(server_label, 0, 1, 0, 1, ypadding = 10)
		self.server_entry = gtk.Entry()
		conn_table.attach(self.server_entry, 1, 2, 0, 1, xpadding = 10)
		self.custom_port_cb = gtk.CheckButton( _("Use custom port") )
		self.custom_port_cb.connect("toggled", self.on_custom_port_cb_toggled)

		conn_table.attach(self.custom_port_cb, 0, 1, 1, 2, ypadding = 10)
		self.port_sb = gtk.SpinButton()
		self.port_sb.set_sensitive(False)
		self.port_sb.set_range(0, 100000)
		self.port_sb.set_value(143)
		conn_table.attach(self.port_sb, 1, 2, 1, 2, xpadding = 10)
		
		thbox = gtk.HBox(True, 32)
		type_align.add(thbox)

		self.std_radio = gtk.RadioButton( label = _("Standard") )
		self.ssl_radio = gtk.RadioButton( group = self.std_radio, label=_("SSL") )
		self.ssl_radio.connect("toggled", self.on_ssl_toggled)
		thbox.pack_start(self.std_radio, True, True, 0)
		thbox.pack_start(self.ssl_radio, True, True, 0)


		vbox2 = gtk.VBox(True, 20)
		self.imap_default_rb = gtk.RadioButton( label = _("Default inbox") )
		self.imap_other_rb = gtk.RadioButton( group = self.imap_default_rb, 
							label = _("Other") )
		self.imap_other_rb.connect("toggled", self.on_other_mbox_toggled)
		self.imap_mbox_entry = gtk.Entry()
		self.imap_mbox_entry.set_sensitive(False)
		vbox2.pack_start(self.imap_default_rb, True, True, 0)
		hbox2 = gtk.HBox(True)
		vbox2.pack_start(hbox2, True, True, 0)
		hbox2.pack_start(self.imap_other_rb, True, True, 0)
		hbox2.pack_start(self.imap_mbox_entry, True, True, 0)
		
		mailbox_align.add(vbox2)
		
		self.attach(vbox)
	
	def on_other_mbox_toggled(self, tb):
		if tb.get_active():
			self.imap_mbox_entry.set_sensitive(True)
		else:
			self.imap_mbox_entry.set_sensitive(False)
	
	def on_ssl_toggled(self, tb):
		if tb.get_active():
			self.port_sb.set_value(993)
		else:
			self.port_sb.set_value(143)
			
	def on_custom_port_cb_toggled(self, cb):
		if cb.get_active():
			self.port_sb.set_sensitive(True)
		else:
			self.port_sb.set_sensitive(False)


	def add_checker(self):
		username = self.username_entry.get_text().strip()
		password = self.password_entry.get_text().strip()
		server = self.server_entry.get_text().strip()
		ssl = "0"
		if self.ssl_radio.get_active():
			ssl = "1"

		port = str(self.port_sb.get_value_as_int())

		use_default_mbox = "0"
		if self.imap_default_rb.get_active():
			use_default_mbox = "1"
		mbox = self.imap_mbox_entry.get_text()

		if not self.custom_port_cb.get_active():
			if ssl == "1":
				port = "993"
			else:
				port = "143"

		if username != "" and password != "" and server != "":
			dic = {
				"type"			: Info().get_name(),
				"username"		: username, 
				"password"		: password,
				"server"		: server,
				"ssl"			: ssl,
				"port"			: port,
				"use_default_mbox"	: use_default_mbox,
				"mbox"			: mbox,
				"enabled"		: "1"
			}
			account_id = self.amanager.add_account(dic)

	def fill(self, info):
		self.username_entry.set_text(info["username"])
		self.password_entry.set_text(info["password"])
		self.server_entry.set_text(info["server"])
		self.port_sb.set_value(int(info["port"]))

		if info["ssl"] == "0":
			self.std_radio.set_active(True)
		else:
			self.ssl_radio.set_active(True)

		if (info["ssl"] == "0" and info["port"] == "143") or \
			(info["ssl"] == "1" and info["port"] == "993"):
			self.custom_port_cb.set_active(False)
			self.port_sb.set_sensitive(False)
		else:
			self.custom_port_cb.set_active(True)
		
		if info["use_default_mbox"] == "1":
			self.imap_mbox_entry.set_sensitive(False)
		self.imap_mbox_entry.set_text(info["mbox"])

	def update_checker(self, info):
		username = self.username_entry.get_text().strip()
		password = self.password_entry.get_text().strip()
		server = self.server_entry.get_text().strip()
		ssl = "0"
		if self.ssl_radio.get_active():
			ssl = "1"

		port = str(self.port_sb.get_value_as_int())

		use_default_mbox = "0"
		if self.imap_default_rb.get_active():
			use_default_mbox = "1"
		mbox = self.imap_mbox_entry.get_text()

		if not self.custom_port_cb.get_active():
			if ssl == "1":
				port = "993"
			else:
				port = "143"

		if username != "" and password != "" and server != "":
			dic = {
				"type"			: Info().get_name(),
				"username"		: username, 
				"password"		: password,
				"server"		: server,
				"ssl"			: ssl,
				"port"			: port,
				"use_default_mbox"	: use_default_mbox,
				"mbox"			: mbox,
				"enabled"		: info["enabled"]
			}
			self.amanager.update_account(info["id"], dic)


class Checker(BaseChecker):
	def __init__(self, account):
		account_id = account['id']
		BaseChecker.__init__(self, account_id)

		if account["type"] != Info().get_name(): 
			self.set_inited(False)
			return

		self.username = account["username"]
		if not account.has_key("server"):
			print "Warnig: bad configration"
			return

		self.server = account["server"]
		ssl = False
		if account.has_key("ssl"):
			if account["ssl"] == "1":
				ssl = True
			elif account["ssl"] == "0":
				ssl = False
		
		self.port = 143 # default imap port
		if account.has_key("port"):
			self.port = account["port"]
		
		self.notified = []
		
		self.use_default_mbox = True
		self.mbox_dir = None
		if account["use_default_mbox"] == "0":
			self.use_default_mbox = False
			self.mbox_dir = account["mbox"]

		password = account['password']
		self.imapbox = ImapBox(self.username, password, 
						self.server, self.port, ssl,
						self.use_default_mbox, self.mbox_dir)
		
		self.set_inited(True)
	
	def get_notifier_actions(self):
		def openmr():
			utils.open_mail_reader()
		acts = {
			"mailreader" : (openmr, _("Open mail reader"))
		}
		return acts
					
	def reset(self):
		self.notified = []
	
	def update_info(self, account):
		self.username = account["username"]
		password = account["password"]
		ssl = account["ssl"]
		self.server = account["server"]
		self.port = account["port"]
		ssl = False
		if account.has_key("ssl"):
			if account["ssl"] == "1":
				ssl = True
			elif account["ssl"] == "0":
				ssl = False
		mbox_dir = None
		if account["use_default_mbox"] == "1":
			use_default_mbox = True
		else:
			use_default_mbox = False
			mbox_dir = account["mbox"]
			
		del self.imapbox
		self.imapbox = ImapBox(self.username, password, 
						self.server, self.port, ssl,
						use_default_mbox, mbox_dir)

	def check(self):
		# prevent recalling
		if self.checking: return
		self.start_check()

		print "BEGIN checking imap account %s@%s ..." % (self.username, self.server)
		try:
			# each mail in mail: [subject, from, msgid]
			mails = self.imapbox.get_mails()
		except ImapBoxConnectionError:
			err = _("IMAP Error")
			msg = _("Error while connecting to %s on port %s") % (self.server, 
										self.port)
			self.emit("checking-error", error, msg, True)
			self.stop_check()
			return
		except ImapBoxAuthError:
			err = _("IMAP Auth Error")
			msg = _("Invalid Username or password for account %s@%s") % (self.username, 
										self.server)
			
			self.emit("checking-error", error, msg, True)
			self.stop_check()
			return

		count = len(mails)
		returnlist = []
		
		if count > 1:
			status_title = _("There are %s new mails") % count
		else:
			status_title = _("There is a new mail")


		if count == 0:
			self.stop_check()
			if len(self.notified) != 0:
				self.emit("status-changed", 
					count, 
					status_title,
					"",
					None
				)
			print "END checking imap account %s@%s ..." % (self.username, self.server)
			return

		if count < MAX_NOTIFIED_MAILS:
			loop = count
		else:
			loop = MAX_NOTIFIED_MAILS

		tmp = 0
		
		status_changed = False
		mails.reverse()
		ids = []
		for mail in mails:
			msgid = mail[2]
			ids.append(msgid)
			if msgid not in self.notified:
				status_changed = True
				if tmp <= loop:
					try:
						subject = mail[0]
						author = mail[1]
						returnlist.append([author, subject])
					except:
						print "Warning: imapchecker cannot display the message"
					tmp += 1
				self.notified.append(msgid)
		
		# I read a message
		for msg in self.notified:
			if msg not in ids:
				status_changed = True
				self.notified = ids
				break


		if status_changed:
			self.emit("status-changed", 
					count, 
					status_title,
					self.__build_message(returnlist),
					None
				)
		self.stop_check()

		print "END checking imap account %s@%s ..." % (self.username, self.server)
	
	def __build_message(self, mails):
		subject_i18n = _(u"<b>Subject:</b>")
		author_i18n = _(u"<b>From:</b>")
		message = u""
		mailbox = "%s@%s" % (self.username, self.server)
		mailboxname = _("<b>Box:</b> %s") % mailbox
		for author, subject in mails:
			try:
				author = unicode(author, "utf-8")
			except UnicodeDecodeError:
				author = unicode(author, "latin-1", "replace")
			except:
				pass
			try:
				subject = unicode(subject, "utf-8")
			except UnicodeDecodeError:
				subject = unicode(subject, "latin-1", "replace")
			except:
				pass
			try:
				message += u"%s %s\n%s %s\n%s\n\n" % \
					(subject_i18n, subject,
					author_i18n, author, mailboxname)
			except:
				message += u"%s %s\n%s %s\n%s\n\n" % \
					(subject_i18n, _("Unknown"),
					author_i18n, _("Unknown"), mailboxname)
		
		return message


