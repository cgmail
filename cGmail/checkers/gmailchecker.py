#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import urllib2 
import urllib
import gtk

from lib.common import *
from base import *
from lib import feedparser

from lib.common import ICONS_BASE_PATH
from lib import utils

GMAIL_ATOM_URL = "https://mail.google.com/gmail/feed/atom"
# to check labels we must use GMAIL_ATOM_URL + "/" + label_name
GMAIL_URL = "https://mail.google.com"

class Info(BaseInfo):
	def get_name(self): 
		return "gmail"
	
	def get_icon(self):
		return ICONS_BASE_PATH + "gmail.png"
	
	def get_sensible_data_list(self):
		return ["username", "password"]

class Gui(BaseGui):
	def __init__(self, amanager, base_widg):
		BaseGui.__init__(self, amanager, base_widg)

		settings_frame = gtk.Frame( _("<b>Account Settings</b>") )
		label1 = settings_frame.get_label_widget()
		label1.set_use_markup(True)
		#settings_frame.set_border_width(8)
		settings_frame.set_shadow_type(gtk.SHADOW_NONE)
		settings_align = gtk.Alignment()
		settings_align.set_padding(0, 0, 20, 0)
		settings_frame.add(settings_align)

		labels_frame = gtk.Frame( _("<b>Also check this Labels:</b>") )
		label2 = labels_frame.get_label_widget()
		label2.set_use_markup(True)
		#settings_frame.set_border_width(8)
		labels_frame.set_shadow_type(gtk.SHADOW_NONE)
		#labels_align = gtk.Alignment()
		#labels_align.set_padding(0, 0, 20, 0)
		#labels_frame.add(labels_align)


		hbox = gtk.HBox(True, 20)
		vbox2 = gtk.VBox(spacing = 20)
		sw = gtk.ScrolledWindow()
	        sw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
	        sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

		self.labels_treeview = gtk.TreeView()
		self.labels_treeview.set_headers_visible(False)
		sw.add(self.labels_treeview)
		self.labels_store = gtk.ListStore(gobject.TYPE_STRING)
		
		name_cell_renderer = gtk.CellRendererText()
		name_cell_renderer.set_property("editable", True)
		name_cell_renderer.connect("edited", self.on_label_name_edited)
		label_name_column = gtk.TreeViewColumn( _("Label"),
			name_cell_renderer, text = 0)
		self.labels_treeview.append_column(label_name_column)
		self.labels_treeview.set_model(self.labels_store)

		vbox2.pack_start(sw, True, True, 0)
		vbox2.pack_start(hbox, True, True, 0)

		add_button = gtk.Button(stock = gtk.STOCK_ADD)
		add_button.connect("clicked", self.on_label_add)
		remove_button = gtk.Button(stock = gtk.STOCK_REMOVE)
		remove_button.connect("clicked", self.on_label_remove)
		hbox.pack_start(add_button, False, False, 0)
		hbox.pack_start(remove_button, False, False, 0)
		#labels_align.add(vbox2)
		labels_frame.add(vbox2)

		vbox = gtk.VBox(False)
		table = gtk.Table(2, 2, False)
		table.set_row_spacings(10)
		table.set_col_spacings(10)
		settings_align.add(table)

		username_label = gtk.Label( _("Username: ") )
		table.attach(username_label, 1, 2, 1, 2, ypadding = 10)
		self.username_entry = gtk.Entry()
		table.attach(self.username_entry, 2, 3, 1, 2, xpadding = 10)
		password_label = gtk.Label( _("Password: ") )
		table.attach(password_label, 1, 2, 2, 3, ypadding = 10)
		self.password_entry = gtk.Entry()
		self.password_entry.set_visibility(False)
		table.attach(self.password_entry, 2, 3, 2, 3, xpadding = 10)

		vbox.pack_start(settings_frame)
		vbox.pack_start(labels_frame, True, True, 20)
		#vbox.pack_start(vbox2)
		#empty = gtk.Label()
		#vbox.pack_end(empty, True, True, 0)

		self.attach(vbox)
	
	def on_label_name_edited(self, cell, path, new_text):
                iter = self.labels_store.get_iter((int(path),))
                self.labels_store.set_value(iter, 0, new_text)
	
	def on_label_add(self, arg):
		iter = self.labels_store.append()
		self.labels_store.set_value(iter, 0, _("Type the label name here"))
		selection = self.labels_treeview.get_selection()
		selection.select_iter(iter)
	
	def on_label_remove(self, arg):
		selection = self.labels_treeview.get_selection()
		model, path_list = selection.get_selected_rows()
		path = path_list[0]
		iter = model.get_iter(path)
		self.labels_store.remove(iter)
	
	def add_checker(self):
		username = self.username_entry.get_text().strip()
		password = self.password_entry.get_text().strip()
		
		labels = []
		def add(model, path, iter, labels):
			label = model.get_value(iter, 0)
			labels.append(label)

		self.labels_store.foreach(add, labels)
		labels_string = ""
		for label in labels:
			labels_string += label + ","
		labels_string = labels_string[:len(labels_string)-1]


		if username != "" and password != "":
			dic = {
				"type"		: Info().get_name(),
				"username"	: username, 
				"password"	: password,
				"enabled"	: "1",
				"labels"	: labels_string
			}
			
			self.account_id = self.amanager.add_account(dic)

	def fill(self, info):
		self.account_id = info["id"]
		labels = [l.strip() for l in info["labels"].split(",")]
		for label in labels:
			if label != '':
				iter = self.labels_store.append()
				self.labels_store.set_value(iter, 0, label)

		self.username_entry.set_text(info["username"])
		self.password_entry.set_text(info["password"])
	
	def update_checker(self, info):
		username = self.username_entry.get_text().strip()
		password = self.password_entry.get_text().strip()
		
		labels = []
		def add(model, path, iter, labels):
			label = model.get_value(iter, 0)
			labels.append(label)

		self.labels_store.foreach(add, labels)
		labels_string = ""
		for label in labels:
			labels_string += label + ","
		labels_string = labels_string[:len(labels_string)-1]


		if username != "" and password != "":
			dic = {
				"type"		: Info().get_name(),
				"username"	: username, 
				"password"	: password,
				"enabled"	: info["enabled"],
				"labels"	: labels_string
			}
			self.amanager.update_account(info["id"], dic)


class AtomException(Exception): pass


class Checker(BaseChecker):
	def __init__(self, account):
		self.account_id = account["id"]
		BaseChecker.__init__(self, self.account_id)
		if account["type"] != Info().get_name(): 
			self.set_inited(False)
			return

		self.username = account["username"]
		self.password = account["password"]
		self.labels = []
		for l in account["labels"].split(","):
			l.strip()
			if l != '':
				self.labels.append(l)

		self.notified = []

		self.set_inited(True)	
	
#	def get_name(self):
#		return "%s@gmail.com" % self.username

	def reset(self):
		# do not reset while checking
		if self.checking: return
		self.notified = []

	def getfeed(self, label = None):
		ah = urllib2.HTTPBasicAuthHandler()
		ah.add_password('New mail feed', GMAIL_URL, \
					self.username, self.password)
		op = urllib2.build_opener(ah)
		urllib2.install_opener(op)
		atom_url = GMAIL_ATOM_URL
		if label is not None:
			atom_url += "/" + urllib.quote(label)
		print atom_url

		res = urllib2.urlopen(atom_url)
		return ''.join(res.readlines())
	
	def update_info(self, account):
		self.username = account["username"]
		self.password = account["password"]
		self.labels = []
		for l in account["labels"].split(","):
			l.strip()
			if l != '':
				self.labels.append(l)

	def openbrowser(self):
		url = GMAIL_URL
		if self.username.find('@') != -1:
			domain = self.username.split('@')[1]
			if domain != 'gmail.com':
				url += '/a/' + domain
		utils.open_browser(url)

	def get_notifier_actions(self):
		acts = {
			"openbrowser" : (self.openbrowser, _("Go to Gmail Account"))
		}
		return acts
	
	def get_atom(self, label = None):
		try:
			feed = self.getfeed(label)
		except urllib2.HTTPError, e:
			if str(e) == "HTTP Error 401: Unauthorized":
				msg = _("Invalid username or password! Please check your settings!")
			else:
				msg = str(e)
			
			error = _("Gmail Error on account %s") % self.username
			self.emit("checking-error", error, msg, True)
			#self.stop_check()
			#return
			raise AtomException()
		except Exception, e:
			print "Warning:", e
			self.emit("checking-error", "Warning", str(e), False)
			#self.stop_check()
			#return
			raise AtomException()

		try:
			atom = feedparser.parse(feed)
		except Exception, detail:
			print "Warning: Exception while parsing gmail feeds", detail
			self.emit("checking-error", 
				_("Gmail Warning"), 
				_("Exception while parsing gmail feeds"),
				False)
			#self.stop_check()
			#return
			raise AtomException()

		return atom

	
	def check(self):
		"""
		Check for new mails.
		"""
		# prevent recalling
		if self.checking: return
		self.start_check()

		print "BEGIN checking gmail account %s ..." % self.username
		if len(self.labels) > 0:
			print "\t also checking labels:", self.labels

		try:
			inbox_atom = self.get_atom()
		except AtomException:
			self.stop_check()
			return
		
		labeled_atoms = {}
		if len(self.labels) > 0:
			for label in self.labels:
				try:
					tmp = self.get_atom(label)
					labeled_atoms[label] = tmp
				except AtomException:
					self.stop_check()
					return
		count = len(inbox_atom.entries)
		for tmp in labeled_atoms.keys():
			count += len(labeled_atoms[tmp].entries)
		
		mails = []
		
		if count > 1:
			status_title = _("There are %s new mails") % count
		else:
			status_title = _("There is a new mail")


		if count == 0:
			self.stop_check()
			if len(self.notified) != 0:
				self.emit("status-changed", 
					count, 
					status_title,
					"",
					None
				)
			print "END checking gmail account %s ..." % self.username
			return

		if count < MAX_NOTIFIED_MAILS:
			loop = count
		else:
			loop = MAX_NOTIFIED_MAILS
		
		status_changed = False
		ids = []
		i = 0
		for j in range(len(inbox_atom.entries)):
			i += 1
			if i > loop: break
			link = inbox_atom.entries[j].link
			tmp = link[link.find("message_id"):]
			message_id =  tmp[:tmp.find("&")].split("=")[1]
			ids.append(message_id)
			if message_id not in self.notified:
				status_changed = True
				self.notified.append(message_id)
				title  = inbox_atom.entries[j].title
				author = inbox_atom.entries[j].author
				mails.append([author, title, None])

		labeled_mails = []
		for label in labeled_atoms:
			atom = labeled_atoms[label]
			for j in range(len(atom.entries)):
				i += 1
				if i > loop: break
				link = atom.entries[j].link
				tmp = link[link.find("message_id"):]
				message_id =  tmp[:tmp.find("&")].split("=")[1]
				ids.append(message_id)
				if message_id not in self.notified:
					status_changed = True
					self.notified.append(message_id)
					title  = atom.entries[j].title
					author = atom.entries[j].author
					labeled_mails.append([author, title, label])

		# I read a message
		for msg in self.notified:
			if msg not in ids:
				status_changed = True
				self.notified = ids
				break
		
		message = self.__build_message(mails) + "\n" + self.__build_message(labeled_mails)
		if status_changed:
			self.emit("status-changed", 
					count, 
					status_title,
					message,
					None
				)
		self.stop_check()
		print "END checking gmail account %s ..." % self.username
	
	def __build_message(self, mails):
		subject_i18n = _(u"<b>Subject:</b>")
		author_i18n = _(u"<b>From:</b>")
		label_i18n = _(u"<b>Label:</b>")
		message = u""
		mailbox = "%s@gmail.com" % self.username
		mailboxname = _("<b>Box:</b> %s") % mailbox
		for author, subject, label in mails:
			try:
				author = unicode(author, "utf-8")
			except UnicodeDecodeError:
				author = unicode(author, "latin-1", "replace")
			except:
				pass
			try:
				subject = unicode(subject, "utf-8")
			except UnicodeDecodeError:
				subject = unicode(subject, "latin-1", "replace")
			except:
				pass
			if label is not None:
				try:
					label = unicode(subject, "utf-8")
				except UnicodeDecodeError:
					label = unicode(subject, "latin-1", "replace")
				except:
					pass
			
			if label is not None:
				try:
					message += u"%s %s\n%s %s\n%s %s\n%s\n\n" % \
						(subject_i18n, subject,
						author_i18n, author, 
						label_i18n, label,
						mailboxname)
				except:
					message += u"%s %s\n%s %s\n%s %s\n%s\n\n" % \
						(subject_i18n, _("Unknown"),
						author_i18n, _("Unknown"), 
						label_i18n, label,
						mailboxname)
			else:
				try:
					message += u"%s %s\n%s %s\n%s\n\n" % \
						(subject_i18n, subject,
						author_i18n, author, mailboxname)
				except:
					message += u"%s %s\n%s %s\n%s\n\n" % \
						(subject_i18n, _("Unknown"),
						author_i18n, _("Unknown"), mailboxname)
		
		return message



