#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import gtk
import gtk.glade
import os
import shutil

from lib.common import *
from lib.gconfhelper import GconfHelper
from lib.accountmanager import *
from service.dbusinterface import get_dbus_interface as get_service_dbus_iface

GLADE_FILE = os.path.join(GLADE_BASE_PATH, "preferences_dialog.glade")


class PreferencesDialog:
	def __init__(self):
		self.ignore_signals = True
		self.account_manager = AccountManager()

		self.widgets = gtk.glade.XML(GLADE_FILE, domain = "cgmail")

		dict = {
			"on_display_notif_cb_toggled": self.on_display_notif_toggled,
			"on_play_snd_cb_toggled" : self.on_play_snd_toggled,
			"on_autostart_cb_toggled" : self.on_autostart_cb_toggled,
			"on_gnome_keyring_rb_toggled" : self.on_use_gnome_keyring,
			"on_always_status_icon_cb_toggled" : self.on_always_status_icon,
			"on_command_check_toggled" : self.on_command_check,
			"on_errors_notify_cb_toggled" : self.on_notify_errors,
			"on_plain_file_rb_toggled" : self.on_use_plain_file,
			"on_check_interval_sp_value_changed" : self.on_check_interval_changed
		}

		self.command_entry = self.widgets.get_widget("command_entry")

		self.widgets.signal_autoconnect(dict)
		self.dialog = self.widgets.get_widget("dialog")
		self.gconf_helper = GconfHelper()

		display_notif_cb = self.widgets.get_widget("display_notif_cb")
		value = self.gconf_helper.get_key("display_notifications")
		display_notif_cb.set_active(value)
		
		always_status_icon_cb = self.widgets.get_widget("always_status_icon_cb")
		value = self.gconf_helper.get_key("always_show_status_icon")
		always_status_icon_cb.set_active(value)
		
		play_snd_cb = self.widgets.get_widget("play_snd_cb")
		value = self.gconf_helper.get_key("play_sounds_on_new_mails")
		play_snd_cb.set_active(value)
		
		errors_notify_cb = self.widgets.get_widget("errors_notify_cb")
		value = self.gconf_helper.get_key("notify_errors")
		errors_notify_cb.set_active(value)

		command_check = self.widgets.get_widget("command_check")
		value = self.gconf_helper.get_key("exec_command")
		command_check.set_active(value)
		self.command_entry.set_sensitive(value)

		value = self.gconf_helper.get_key("new_mail_command")
		self.command_entry.set_text(value)
		
		self.gnome_keyring_rb = self.widgets.get_widget("gnome_keyring_rb")
		self.plain_file_rb = self.widgets.get_widget("plain_file_rb")
		value = self.gconf_helper.get_key("use_gnome_keyring")
		if value:
			self.gnome_keyring_rb.set_active(True)
		else:
			self.plain_file_rb.set_active(True)
		
		check_interval_sp = self.widgets.get_widget("check_interval_sp")
		value = self.gconf_helper.get_key("check_interval")
		check_interval_sp.set_value(value)

		self.autostart_file = os.path.expanduser("~/.config/autostart/cgmailservice.desktop")
		autostart_cb = self.widgets.get_widget("autostart_cb")
		value =  os.path.exists(self.autostart_file)
		autostart_cb.set_active(value)

		self.ignore_signals = False

		self.run()

	
	def run(self):
		result = self.dialog.run()
		cmd = self.command_entry.get_text()
		self.gconf_helper.set_key("new_mail_command", cmd)
		self.dialog.destroy()
	
	def on_autostart_cb_toggled(self, w):
		if self.ignore_signals: return

		if w.get_active():
			if not os.path.exists(self.autostart_file):
				try:
					os.makedirs(os.path.expanduser("~/.config/autostart"))
				except OSError:
					pass

				try:
					shutil.copyfile(CGMAIL_AUTOSTART_FILE, 
							self.autostart_file)
					os.chmod(self.autostart_file, 0700)
				except IOError:
					print "Warning: cannot write to path", self.autostart_file
		else:
			if os.path.exists(self.autostart_file):
				try:
					os.unlink(self.autostart_file)
				except:
					print "Warning: cannot delete", self.autostart_file

	def on_command_check(self, w):
		if self.ignore_signals: return
		self.command_entry.set_sensitive(w.get_active())
		if w.get_active():
			self.gconf_helper.set_key("exec_command", True)
			cmd = self.command_entry.get_text()
			self.gconf_helper.set_key("new_mail_command", cmd)
		else:
			self.gconf_helper.set_key("exec_command", False)
			self.gconf_helper.set_key("new_mail_command", "")
	
	def on_display_notif_toggled(self, w):
		if self.ignore_signals: return

		if w.get_active():
			self.gconf_helper.set_key("display_notifications", True)
		else:
			self.gconf_helper.set_key("display_notifications", False)
	
	def on_play_snd_toggled(self, w):
		if self.ignore_signals: return

		if w.get_active():
			self.gconf_helper.set_key("play_sounds_on_new_mails", True)
		else:
			self.gconf_helper.set_key("play_sounds_on_new_mails", False)
	
	def on_notify_errors(self, w):
		if self.ignore_signals: return

		if w.get_active():
			self.gconf_helper.set_key("notify_errors", True)
		else:
			self.gconf_helper.set_key("notify_errors", False)


	def on_use_gnome_keyring(self, w):
		if self.ignore_signals: return

		if w.get_active():
			try:
				self.gconf_helper.set_key("use_gnome_keyring", True)
				self.account_manager.reset_use_gnome_keyring()
			except CannotSetDefault:
				# We can pass here becouse if set_keyring_default
				# will raise the exception, the set_key method will
				# not be called
				self.ignore_signals = True
				self.plain_file_rb.set_active(True)
				self.ignore_signals = False
	
	def on_use_plain_file(self, w):
		if self.ignore_signals: return

		if w.get_active():
			try:
				self.gconf_helper.set_key("use_gnome_keyring", False)
				self.account_manager.reset_use_gnome_keyring()
			except CannotSetDefault:
				# same as on_use_gnome_keyring
				self.ignore_signals = True
				self.gnome_keyring_rb.set_active(True)
				self.ignore_signals = False
	
	def on_always_status_icon(self, w):
		if self.ignore_signals: return

		iface = get_service_dbus_iface()

		if w.get_active():
			self.gconf_helper.set_key("always_show_status_icon", True)
		else:
			self.gconf_helper.set_key("always_show_status_icon", False)
	
	def on_check_interval_changed(self, w):
		if self.ignore_signals: return

		value = w.get_value_as_int()
		self.gconf_helper.set_key("check_interval", value)
