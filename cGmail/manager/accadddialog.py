#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import gtk
import gtk.glade
import os
import gobject

from lib.common import *
from lib.checkersutils import *

GLADE_FILE = os.path.join(GLADE_BASE_PATH, "account_add_dialog.glade")


(
 ICON_COL,
 NAME_COL,
) = range(2)


class AccountAddDialog:
	def __init__(self, amanager):
		self.widgets = gtk.glade.XML(GLADE_FILE, domain = "cgmail")

		self.amanager = amanager

		dict = {
		#	"on_checkers_listview_row_activated": self.on_checker_selected
			"on_type_box_changed" : self.on_type_box_changed
		}

		self.widgets.signal_autoconnect(dict)
		self.dialog = self.widgets.get_widget("dialog")

		self.checkers_liststore = gtk.ListStore(
						 gtk.gdk.Pixbuf,
						 gobject.TYPE_STRING
						)
		
		self.typebox = self.widgets.get_widget("type_box")
                self.typebox.set_model(self.checkers_liststore)

                self.type_pixcell = gtk.CellRendererPixbuf()
                self.type_textcell = gtk.CellRendererText()
                self.typebox.pack_start(self.type_pixcell, True)
                self.typebox.pack_end(self.type_textcell, True)
                self.type_pixcell.set_property( 'xalign', 0.0 )

                self.typebox.add_attribute(self.type_pixcell, 'pixbuf', 0)
                self.typebox.add_attribute(self.type_textcell, 'text', 1)

		self.fill_checkers_list()


		self.gui_base_widget = self.widgets.get_widget("properties_base_widget")

		# checker properties gui
		self.cgui = None

		self.typebox.set_active(0)
		#self.checkers_listview.emit("row-activated", path, None)

		#self.dialog.resize(600, 500)
	
	def on_type_box_changed(self, *args):
		"""
		Build configuration gui for selected checker
		"""
		model = self.typebox.get_model()
		iter = self.typebox.get_active_iter()
		name = model.get_value(iter, 1)
		checker = get_checker_by_name(name)
		child = self.gui_base_widget.get_child()
		if child is not None:
			child.destroy()
		self.cgui = checker.Gui(self.amanager, self.gui_base_widget)

	def fill_checkers_list(self):
		checkers = load_checkers()
		for c in checkers:
			info = c.Info()
			icon_path = info.get_icon()
			icon = gtk.gdk.pixbuf_new_from_file(icon_path)
			name = info.get_name()
			
			iter = self.checkers_liststore.append()
			self.checkers_liststore.set_value(iter, ICON_COL, icon)
			self.checkers_liststore.set_value(iter, NAME_COL, name)

	
	def run(self):
		result = self.dialog.run()

		if result == gtk.RESPONSE_OK:
			if self.cgui is not None:
				self.cgui.add_checker()
		
		if result == gtk.RESPONSE_OK or result == gtk.RESPONSE_CANCEL:
			self.dialog.destroy()

		# no response. Close button clicked on the dialog window
		self.dialog.destroy()

if __name__ == "__main__":
	from accountmanager import AccountManager
	a = AccountManager()
	AccountPropertiesDialog(a).run()
	gtk.main()

