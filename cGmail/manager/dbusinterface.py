#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import dbus
import dbus.service
import dbus.mainloop.glib

class ManagerDbusInterface(dbus.service.Object):
	def __init__(self, bus, obj):
		dbus.service.Object.__init__(self, bus, obj)

		self.on_refreshing_cb = None

	@dbus.service.method("org.tuxfamily.cgmail.ManagerInterface",
				in_signature="sb", out_signature="")
	def set_refreshing(self, account_id, value):
		if self.on_refreshing_cb is not None:
			self.on_refreshing_cb(account_id, value)
	
	def set_on_refreshing_cb(self, cb):
		self.on_refreshing_cb = cb

getting = False

def get_dbus_interface():
	global getting
	if getting: return None

	#return None

	dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
	bus = dbus.SessionBus()
	try:
		obj = bus.get_object("org.tuxfamily.cgmail.Manager", "/Manager")
		iface = dbus.Interface(obj, "org.tuxfamily.cgmail.ManagerInterface")
	except dbus.DBusException:
		getting = False
		return None

	getting = False
	return iface

