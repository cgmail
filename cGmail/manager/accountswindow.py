#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import gtk
import gtk.glade
import os
import gobject
import dbus

from lib.common import *
from accadddialog import AccountAddDialog
from lib.accountmanager import AccountManager
from service.dbusinterface import get_dbus_interface as get_service_dbus_iface
from preferencesdialog import PreferencesDialog
from lib import utils
from lib.checkersutils import *

from dbusinterface import ManagerDbusInterface

GLADE_FILE = os.path.join(GLADE_BASE_PATH, "accounts_window.glade")

POPUPMENU_GLADE_FILE = os.path.join(GLADE_BASE_PATH, "accounts_treeview_popupmenu.glade")

(
 ENABLED_COL,
 ICON_COL,
 NAME_COL,
 REFRESHING_COL,
 ID_COL
) = range(5)

def on_url(d, link, data):
	utils.open_browser(link)
gtk.about_dialog_set_url_hook(on_url, None)

gtk.gdk.threads_init()

class AccountsWindow:
	def __init__(self):
		gtk.window_set_default_icon_from_file(NOMAIL_ICON)

		self.amanager = AccountManager()

		xml = gtk.glade.XML(POPUPMENU_GLADE_FILE, domain="cgmail")
		self.treeview_popupmenu = xml.get_widget("menu")
		dict = {
			"on_properties_activate" : self.on_properties_activate,
			"on_remove_activate" : self.on_remove_activate
		}
		xml.signal_autoconnect(dict)

		self.widgets = gtk.glade.XML(GLADE_FILE, domain = "cgmail")

		dict = {
			"on_remove_activate" : self.on_remove_activate,
			"on_new_activate" : self.on_add_button_clicked,
			"on_exit_activate" : self.on_exit,
			"on_add_button_clicked" : self.on_add_button_clicked,
			"on_refresh_button_clicked" : self.on_refresh_button_clicked,
			"on_properties_activate" : self.on_properties_activate,
			"on_about_activate" : self.on_about_activate,
			"on_start_stop_button_toggled" : self.on_start_stop_button_toggled,
			"on_preferences_activate" : self.on_preferences_activate,
			"on_mailboxes_treeview_button_press_event" : self.on_mailboxes_treeview_button_press,
			"on_mailboxes_treeview_row_activated" : self.on_mailboxes_treeview_row_activated
		}
		

		self.widgets.signal_autoconnect(dict)
		self.window = self.widgets.get_widget("window")

		self.mailboxes = self.widgets.get_widget("mailboxes_treeview")

		self.mailboxes_store = gtk.ListStore(
						 gobject.TYPE_BOOLEAN,
						 gtk.gdk.Pixbuf,
						 gobject.TYPE_STRING,
						 gtk.gdk.Pixbuf,
						 gobject.TYPE_STRING)

		self.refreshing_icon = self.window.render_icon(gtk.STOCK_REFRESH, 
									gtk.ICON_SIZE_MENU)
		
		self.ignore_events = True
		self.start_stop_button = self.widgets.get_widget("start_stop_button")
		self.statusbar = self.widgets.get_widget("statusbar")
		self.statusbar_context_id = self.statusbar.get_context_id("default")
		iface = get_service_dbus_iface()
		if iface is not None:
			self.start_stop_button.set_active(True)
			self.statusbar.pop(self.statusbar_context_id)
			self.statusbar.push(self.statusbar_context_id, _("cGmail service is active") )
		else:
			self.start_stop_button.set_active(False)
			self.statusbar.pop(self.statusbar_context_id)
			self.statusbar.push(self.statusbar_context_id, _("cGmail service is not active!") )

		self.ignore_events = False
		
		renderer = gtk.CellRendererToggle()
		renderer.connect("toggled", self.on_account_set_active)
		self.enabled_column = gtk.TreeViewColumn( _("Enabled"),
			renderer, active = ENABLED_COL)
		self.mailboxes.append_column(self.enabled_column)

		self.icon_column = gtk.TreeViewColumn( _("Type"),
			gtk.CellRendererPixbuf(), pixbuf = ICON_COL)
		self.mailboxes.append_column(self.icon_column)
		
		self.name_column = gtk.TreeViewColumn( _("Name"),
			gtk.CellRendererText(), text = NAME_COL)
		self.mailboxes.append_column(self.name_column)

		self.refreshing_column = gtk.TreeViewColumn( "",
			gtk.CellRendererPixbuf(), pixbuf = REFRESHING_COL)
		self.mailboxes.append_column(self.refreshing_column)
		
		self.mailboxes.set_model(self.mailboxes_store)

		self.show_accounts_list()

	
	def show_accounts_list(self):
		accounts = self.amanager.get_accounts_dicts()
		if accounts is None: return
		if len(accounts) == 0: return

		self.mailboxes_store.clear()

		for account in accounts:
			iter = self.mailboxes_store.append()
			try:
				text = account["username"]

				info = get_checker_info_by_name(account["type"])
				icon_path = info.get_icon()
				icon = gtk.gdk.pixbuf_new_from_file(icon_path)

				self.mailboxes_store.set_value(iter, ICON_COL, icon)
				self.mailboxes_store.set_value(iter, NAME_COL, text)
				self.mailboxes_store.set_value(iter, ID_COL, 
									account["id"])

				enabled = account["enabled"]

				self.ignore_events = True
				if enabled == "0":
					self.mailboxes_store.set_value(iter, ENABLED_COL, False)
				else:
					self.mailboxes_store.set_value(iter, ENABLED_COL,True)
				self.ignore_events = False
			except Exception, details:
				print "Ignoring account:", details

	def on_start_stop_button_toggled(self, butt):
		if self.ignore_events: return

		if butt.get_active():
			if not utils.invoke_subprocess("cgmailservice"):
				utils.invoke_subprocess("./cgmailservice.py")
			self.statusbar.pop(self.statusbar_context_id)
			self.statusbar.push(self.statusbar_context_id, _("cGmail service is active") )
		else:
			iface = get_service_dbus_iface()
			if iface is not None:
				iface.exit()
			self.statusbar.pop(self.statusbar_context_id)
			self.statusbar.push(self.statusbar_context_id, _("cGmail service is not active!") )
	
	def on_refresh_button_clicked(self, butt):
		iface = get_service_dbus_iface()
		if iface is not None:
			iface.refresh()
	
	def on_preferences_activate(self, arg):
		PreferencesDialog()
	
	def on_about_activate(self, arg):
		img = gtk.Image()
		img.set_from_file(ABOUT_ICON)
		img.show()
		dlg = gtk.AboutDialog()
		dlg.set_version(CGMAIL_VERSION)
		dlg.set_name("cGmail")
		dlg.set_copyright("Copyright (c) 2007,2008 Marco Ferragina")
		dlg.set_logo(img.get_pixbuf())
		def close(w, res):
			 if res == gtk.RESPONSE_CANCEL:
			 	w.hide()
		dlg.connect("response", close)
		dlg.set_license( _("cGmail is free software relased under GPL license terms") )

		dlg.set_website("http://cgmail.tuxfamily.org")
		dlg.set_authors(["Marco Ferragina <marco.ferragina@gmail.com>"])
		dlg.set_translator_credits(TRANSLATORS)
		dlg.run()

	
	def on_account_set_active(self, cell, path):
		if self.ignore_events: return

		model = self.mailboxes_store
		iter = model.get_iter((int(path),))
		enabled = model.get_value(iter, ENABLED_COL)
		id = model.get_value(iter, ID_COL)
		# why not direct logic?
		if enabled:
			self.amanager.disable_account(id)
		else:
			self.amanager.enable_account(id)
		enabled = not enabled
		model.set(iter, ENABLED_COL, enabled)

	def on_mailboxes_treeview_row_activated(self, *args):
		self.on_properties_activate(None)
		pass

	def on_add_button_clicked(self, arg):
		ap = AccountAddDialog(self.amanager)
		#ap.fill(self.amanager)
		ap.run()
		
		self.mailboxes_store.clear()
		self.show_accounts_list()
	
	def on_remove_activate(self, arg):
		selection = self.mailboxes.get_selection()
		model, path_list = selection.get_selected_rows()
		path = path_list[0]
		iter = model.get_iter(path)
		id = model.get_value(iter, ID_COL)
		self.amanager.remove_account(id)
		self.mailboxes_store.clear()
		self.show_accounts_list()
	
	def on_properties_activate(self, arg):
		selection = self.mailboxes.get_selection()
		model, path_list = selection.get_selected_rows()
		try:
			path = path_list[0]
			iter = model.get_iter(path)
			id = model.get_value(iter, ID_COL)
			try:
				info = self.amanager.get_account_info_by_id(id)
				checker = get_checker_by_name(info["type"])
				dialog = gtk.Dialog()
				#dialog.resize(200, 200)
				frame = gtk.Frame()
				frame.set_shadow_type(gtk.SHADOW_NONE)
				frame.set_border_width(10)
				dialog.vbox.add(frame)
				cgui = checker.Gui(self.amanager, frame)
				cgui.fill(info)
				dialog.show_all()
			#	cancel_button = dialog.add_button(gtk.STOCK_CANCEL,
			#				gtk.RESPONSE_CANCEL)
			#	ok_button = dialog.add_button(gtk.STOCK_OK,
			#				gtk.RESPONSE_OK)
				dialog.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)
				result = dialog.run()
				#if result == gtk.RESPONSE_OK:
				if result == gtk.RESPONSE_CLOSE:
					cgui.update_checker(info)
				dialog.destroy()
			except Exception, detail:
				print "Error while filling prop dialog", detail

		except Exception, detail:
			print "Error------------------------------"
			print Exception, detail
			pass
		
		# I need this to update accounts content
		self.show_accounts_list()

	def on_mailboxes_treeview_button_press(self, treeview, event):
		if event.button == 3:
			x = int(event.x)
			y = int(event.y)
			time = event.time
			pthinfo = treeview.get_path_at_pos(x, y)
			if pthinfo is not None:
				path, col, cellx, celly = pthinfo
				treeview.grab_focus()
				treeview.set_cursor( path, col, 0)
				self.treeview_popupmenu.popup(None, None, None, event.button, time)
				#self.popup.popup( None, None, None, event.button, time)
		return 0
	
	def on_exit(self, *args):
		gtk.main_quit()
	
	def set_refreshing(self, account_id, refreshing):
		gtk.gdk.threads_enter()
		model = self.mailboxes_store
		iter = model.get_iter_first()
		id = model.get_value(iter, ID_COL)
		while id != account_id and iter is not None:
			iter = model.iter_next(iter)
			if iter is not None:
				id = model.get_value(iter, ID_COL)
		if refreshing:
			self.mailboxes_store.set_value(iter, REFRESHING_COL, 
						self.refreshing_icon)
		else:
			self.mailboxes_store.set_value(iter, REFRESHING_COL, 
							None)

		gtk.gdk.threads_leave()

	def show(self):
		# start dbus service
		dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
		session_bus = dbus.SessionBus()
		name = dbus.service.BusName("org.tuxfamily.cgmail.Manager", session_bus)
		dbus_service = ManagerDbusInterface(session_bus, '/Manager')
		dbus_service.set_on_refreshing_cb(self.set_refreshing)

		self.window.connect("delete-event", self.on_exit)
		try:
			gtk.main()
		except KeyboardInterrupt:
			pass


