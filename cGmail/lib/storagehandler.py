#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

class StorageHandler:
	def readline(self): 
		"""
		Must return a line for each call. It must return a "not line"
		when and only when no lines remains.
		example:
			line = obj.readline()
			if not line:
				print "not line"
		not line must be printed only at the end of file
		"""
		raise NotImplementedError()

	def write(self, data):
		"""
		write the data into the storage
		"""
		raise NotImplementedError()

	def get_new_writer(self):
		"""
		Return a new writer object. This object must have the write method
		(must be a StorageHandler). Before return the object this method
		must initialize the Storage with empty data
		"""
		raise NotImplementedError()

	def close(self):
		"""
		Finalize the storage. It execute the real writing or
		close the file for a file storage handler
		"""
		raise NotImplementedError()
