#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


import poplib

from email.Parser import Parser as EmailParser

class PopBoxConnectionError(Exception): pass
class PopBoxAuthError(Exception): pass

class PopBox:
	def __init__(self, user, password, host, port = 110, ssl = False):
		self.user = user
		self.password = password
		self.host = host
		self.port = int(port) # ensure int
		self.ssl = ssl

		self.mbox = None
		self.parser = EmailParser()

			
	def __connect(self):
		print "trying to connect to pop3 server %s on port %s..." % \
						(self.host, self.port)
		try:
			if not self.ssl:
				self.mbox = poplib.POP3(self.host, self.port)
			else:
				self.mbox = poplib.POP3_SSL(self.host, self.port)
		except Exception:
			raise PopBoxConnectionError()
		print "...connection done to %s on port %s" % \
						(self.host, self.port)


		print "authenticating user", self.user
		try:
			self.mbox.user(self.user)
			self.mbox.pass_(self.password)
		except poplib.error_proto:
			raise PopBoxAuthError()
		print "user", self.user, "authenticated"

	def get_mails(self):
		try:
			self.__connect()
		except PopBoxConnectionError:
			raise PopBoxConnectionError()
		except PopBoxAuthError:
			raise PopBoxAuthError()

		print "getting mails ..."
		messages = []
		msgs = self.mbox.list()[1]
		print len(msgs), "mails to get"
		for msg in msgs:
			try:
				msgNum = int(msg.split(" ")[0])
				msgSize = int(msg.split(" ")[1])

				# retrieve only the header
				st = "\n".join(self.mbox.top(msgNum, 0)[1])
			#	print st
			#	print "----------------------------------------"
				msg = self.parser.parsestr(st, True) # header only
				sub = msg.get("Subject")
				msgid = msg.get("Message-Id")
				fr = msg.get("From")
				messages.append( [sub, fr, msgid] )
			except poplib.error_proto, e:
				print "Warning: pop3 error", e
		
		self.mbox.quit()
		print "retrieved %s messages" % len(messages)
		return messages

if __name__ == "__main__":
	try:
		mbox = PopBox("", "", "")
		mails = mbox.get_mails()
		print "Ci sono", len(mails), "emails"
		print mails
	except PopBoxConnectionError:
		print "Errore di connessione al server pop"
	except PopBoxAuthError:
		print "Username o password non validi"
