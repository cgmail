#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

from iniman import IniMan

class NoSuchDictException(Exception): pass
class CannotSaveError(Exception): pass
class CorruptedConfiguration(Exception): pass

class DictHelper(IniMan):
	def __init__(self, handler):
		IniMan.__init__(self, handler)
		self.__dicts = self.__retrieve_dicts()

	def __retrieve_dicts(self):
		dicts = []
		sections = self.get_sections()
		for sec in sections:
			dic = self.get_by_section(sec)
			dic["id"] = sec
			dicts.append(dic)
		return dicts
	
	def get_dicts(self):
		self.__dicts = []
		self.__dicts = self.__retrieve_dicts()
		return self.__dicts

	def remove_dict(self, id): 
		dic = None
		for tmp in self.__dicts:
			if tmp["id"] == id:
				dic = tmp
		if dic is None:
			raise NoSuchDictException
		for key, value in dic.iteritems():
			self.remove(id + "." + key)

		self.__retrieve_dicts()

	def add_dict(self, dic, force_id = None):
		if force_id is None:
			sections = self.get_sections()
			maxid = 0
			for sec in sections:
				if int(sec) > maxid:
					maxid = int(sec)
			id = maxid + 1
		else:
			id = force_id
		id = str(id)
		self.__dicts.append(dic)
		for key, value in dic.iteritems():
			self.add(id + "." + key, value)

		return id

	def reset(self):
		sections = self.get_sections()
		for sect in sections:
			self.remove_dict(sect)

