# -*- coding: utf-8 -*-
#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


try:
	import gettext
	import gtk.glade
	import locale
	locale.setlocale(locale.LC_ALL, "")
	gettext.install('cgmail', unicode = 1)
	gtk.glade.bindtextdomain('cgmail')
except (IOError,locale.Error), e:
	print "Warning", e
	import __builtin__
	__builtin__.__dict__['_'] = lambda x : x

CGMAIL_VERSION = "0.5"

TRANSLATORS = """Marco Ferragina <marco.ferragina@gmail.com> (Italian)
Festor Wailon Dacoba (Spanish)
Nicolas Schirrer (French)
Hajo Engelke <heedful@xmail.net> (German)
Tomáš Pikálek (Czech)
"""

BASE_DATA_PATH = "/usr/share/cGmail/"

if not "/site-packages/cGmail/lib/common.py" in __file__:
	# From source
	BASE_DATA_PATH = "data/"


# Show only last MAX_NOTIFIED_MAILS in notifications
# to avoid to overbound the desktop area
MAX_NOTIFIED_MAILS = 5

GLADE_BASE_PATH = BASE_DATA_PATH + "glade/"

#############################################################
#
#    Data Files
#

CGMAIL_AUTOSTART_FILE = BASE_DATA_PATH + "cgmailservice.desktop"

#########
### icons
ABOUT_ICON = BASE_DATA_PATH + "cgmail.svg"
DEFAULT_NOTIFY_ICON = BASE_DATA_PATH + "malert.png"
NOMAIL_ICON = BASE_DATA_PATH + "nomail.png" 
MORE_NEWMAIL_ICON = BASE_DATA_PATH + "more-newmail.png"
NEWMAIL_ICON_BASE_PATH = BASE_DATA_PATH + "%i-newmail.png"

ICONS_BASE_PATH = BASE_DATA_PATH

SND_NOTIFY = BASE_DATA_PATH + "notify.wav"
