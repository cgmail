#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


import os
from ConfigParser import MissingSectionHeaderError

from dicthelper import DictHelper, CannotSaveError, CorruptedConfiguration
from storagehandler import StorageHandler

class FileHandler(StorageHandler):
	def __init__(self):
		self.__file_path = os.path.expanduser("~/.config/cgmail/accounts.ini")
		if not os.path.exists(self.__file_path):
			try:
				os.makedirs(os.path.expanduser("~/.config/cgmail"))
			except OSError:
				pass
		try:
			self.__data = open(self.__file_path).read()
			self.__lines = self.__data.split('\n')
		except IOError:
			self.__lines = []
		self.__count = 0

	def readline(self):
		if self.__count < len(self.__lines):
			line = self.__lines[self.__count]
			self.__count += 1
			if not line:
				return "\n"
			return line
		else:
			return None
	
	def write(self, data):
		self.__f.write(data)
		#f.close()

	def get_new_writer(self):
		self.__f = open(self.__file_path, "w")
		os.chmod(self.__file_path, 0600)
		return self
	
	def close(self):
		self.__f.close()


class FileDictHelper(DictHelper):
	def __init__(self):
		fh = FileHandler()
		try:
			DictHelper.__init__(self, fh)
		except MissingSectionHeaderError:
			fh = fh.get_new_writer()
			fh.close()
			raise CorruptedConfiguration

		
	
		
if __name__ == "__main__":
	f = FileDictHelper()
	print f.get_dicts()
	f.add_dict({"user": "prova"})
	print f.get_dicts()
