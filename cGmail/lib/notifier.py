#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

try:
	import dbus
	from dbus.mainloop.glib import DBusGMainLoop
	HAVE_DBUS = True
except ImportError:
	HAVE_DBUS = False

import gtk
import gobject
import os
import re

from common import *
#import webbrowser

cwd = os.getcwd()

ICON = os.path.join(cwd, DEFAULT_NOTIFY_ICON)
GLADE_FILE = os.path.join(GLADE_BASE_PATH, "popup_notification_window.glade")

notification_instances = 0
class NotificationWindow:
	def __init__(self, title, text, icon = ICON, msec = 3000):
		global notification_instances
		notification_instances += 1

		widgets = gtk.glade.XML(GLADE_FILE, domain="planimo")
		self.window = widgets.get_widget('popup_notification_window')
		close_button = widgets.get_widget('close_button')
		event_type_label = widgets.get_widget('event_type_label')
		
		event_description_label = widgets.get_widget('event_description_label')
		eventbox = widgets.get_widget('eventbox')
		image = widgets.get_widget('notification_image')
		

		event_type_label.set_markup(
			'<span foreground="black" weight="bold">%s</span>' % title)

		self.window.modify_bg(gtk.STATE_NORMAL, 
					gtk.gdk.color_parse('#dab255'))


		bg_color = '#ffffbf'
		popup_bg_color = gtk.gdk.color_parse(bg_color)
		close_button.modify_bg(gtk.STATE_NORMAL, popup_bg_color)
		eventbox.modify_bg(gtk.STATE_NORMAL, popup_bg_color)
		event_description_label.set_markup(
			'<span foreground="black">%s</span>' % text)	
			
		# set the image
		image.set_from_file(icon)
		
		# position the window to bottom-right of screen
		window_width, window_height = self.window.get_size()
		pos_x = gtk.gdk.screen_width() - window_width - 1
		pos_y = gtk.gdk.screen_height() - \
				notification_instances * (window_height + 2)
		self.window.move(pos_x, pos_y)

		widgets.signal_autoconnect(self)
		self.window.show_all()
		gobject.timeout_add(msec, self.on_timeout)

	def on_close_button_clicked(self, widget):
		global notification_instances

		self.window.destroy()
		notification_instances -= 1

	def on_timeout(self):
		global notification_instances

		self.window.destroy()
		notification_instances -= 1

if HAVE_DBUS:
	dbus_loop = DBusGMainLoop()
	session_bus = dbus.SessionBus(mainloop = dbus_loop)
	obj = session_bus.get_object("org.freedesktop.Notifications", 
					"/org/freedesktop/Notifications")
	notif = dbus.Interface(obj, "org.freedesktop.Notifications")

class Notifier:
	def __init__(self):
		global notif
		if HAVE_DBUS:
			self.notif = notif
			self.notif.connect_to_signal("ActionInvoked", self.action_cb)
			self.__actions = {}
			self.__actions_list = []
	
	def action_cb(self, id, act):
		try:
			handler = self.__actions[act][0]
			handler()
		except Exception:
			pass

	def set_actions(self, actions):
		self.__actions = actions
		self.__actions_list = []
		for actname, values in actions.iteritems():
			self.__actions_list.append(actname)
			self.__actions_list.append(values[1])

	def notify(self, title, message, icon = None, msec = 3000):
		if icon is None:
			icon = ICON
		else:
			cwd = os.getcwd()
			icon = os.path.join(cwd, icon)

		iconfile = "file://" + icon
		# allow bold tag and sub nasty chars
		message = message.replace("&", "&amp;")
		message = re.sub(r'<(?!/?b>)', "&lt;", message)
		message = re.sub(r'(?<!b)>', "&gt;", message)

		print message

		if not HAVE_DBUS:
			# Revert to classic window
			NotificationWindow(title, message, icon, msec)
		else:
			# new api
			try:
				self.notif.Notify(
					"cgmail",
					dbus.UInt32(0), 
					iconfile, 
					title, 
					message,
					self.__actions_list, 
					[], 
					dbus.Int32(msec)
					)
			except Exception, detail:
				# Nothing to do with dbus: 
				# Revert to classic window
				print "Warning: Error while trying to use notification-daemon.\n Reverting to classic method.\n Please upgrade your dbus and libnotify version", detail
				NotificationWindow(title, message, icon, msec)


if __name__ == "__main__":
	n = Notifier()
	msg = '<span size="larger">Test</span> ok'
	n.notify("Title", msg, True, msec=10000)

	import gobject
	loop = gobject.MainLoop()
	loop.run()

