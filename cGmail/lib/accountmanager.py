#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import gtk
import gconf
import gnomekeyring

from dicthelper import CorruptedConfiguration
from keyringdicthelper import KeyringDictHelper
from filedicthelper import FileDictHelper
from gconfhelper import GconfHelper

from checkersutils import get_checker_info_by_name


class CannotSetDefault(Exception): pass

class AccountManager:
	def __init__(self):
		self.__keyring_dict_helper = None
		must_reset_keyring = False
		try:
			self.__file_dict_helper = FileDictHelper()
		except CorruptedConfiguration:
			print "Warning: Corrupted configuration. Now I try \
to reset your configuration. This means that you must \
reinsert all your accounts"
			must_reset_keyring = True
			self.__file_dict_helper = FileDictHelper()
			

		self.__use_gnome_keyring = GconfHelper().get_key("use_gnome_keyring")
		if self.__use_gnome_keyring:
			try:
				self.__keyring_dict_helper = KeyringDictHelper()
			except gnomekeyring.DeniedError:
				GconfHelper().set_key("use_gnome_keyring", False)
				self.__file_dict_helper.reset()
				self.__use_gnome_keyring = GconfHelper().get_key("use_gnome_keyring")
			except CorruptedConfiguration:
				print "Warning: Corrupted configuration. Now I try \
to reset your configuration. This means that you must \
reinsert all your accounts"
				self.__file_dict_helper.reset()
				self.__keyring_dict_helper = KeyringDictHelper()

			if must_reset_keyring:
				self.__keyring_dict_helper.reset()

	
	def reset_use_gnome_keyring(self):
		dicts = self.get_accounts_dicts()
		self.__use_gnome_keyring = GconfHelper().get_key("use_gnome_keyring")
		self.__file_dict_helper.reset()
		self.__file_dict_helper = FileDictHelper()

		if self.__use_gnome_keyring:
			print "Enabling gnome keyring ..."
			self.__keyring_dict_helper = KeyringDictHelper()
		else:
			print "Diabling gnome keyring..."
			self.__keyring_dict_helper.reset()
		for account in dicts:
			self.add_account(account)

		print "...done"
	
	def get_accounts_dicts(self): 
		"""
		must return a list of accounts dictionaries
		"""
		dicts = []
		ini_dicts = self.__file_dict_helper.get_dicts()

		if self.__use_gnome_keyring:
			ring_dicts = self.__keyring_dict_helper.get_dicts()
			if len(ring_dicts) == 0:
				# either no accounts are configured or
				# the user have choosen to not give the app
				# privileges to acces to the gnome keyring
				return []
			# we must merge the dicts. mmm cubic complexity. 
			# Better alghoritm?
			#print ini_dicts
			#print ring_dicts

			for i in ini_dicts:
				for r in ring_dicts:
					if r["id"] == i["id"]:
						tmp = i
						for key, value in r.iteritems():
							tmp[key] = value
				dicts.append(tmp)
		else:
			dicts = ini_dicts
			
		return dicts


	def remove_account(self, id):
		if self.__use_gnome_keyring:
			self.__keyring_dict_helper.remove_dict(id)
		
		self.__file_dict_helper.remove_dict(id)
		
	
	def update_account(self, id, account):
		"""
		Update account id with the new dic
		"""
		self.remove_account(id)
		self.add_account(account, force_id = id)
	
	def enable_account(self, id):
		account = self.get_account_info_by_id(id)
		account["enabled"] = 1
		self.update_account(id, account)
	
	def disable_account(self, id):
		account = self.get_account_info_by_id(id)
		account["enabled"] = 0
		self.update_account(id, account)

	def add_account(self, account, force_id = None):
		"""
		Store the account. Secure must be a dict of key => values
		that must be stored using a secure way such gnome keyring. Plain
		is the rest of key => values. When secure is stored, a new id will
		be generated. This id will be used when storing plain so we
		can reconstruct the whole dict
		"""
		c_info = get_checker_info_by_name(account["type"])
		sensible_list = c_info.get_sensible_data_list()
		secure = {}
		plain = {}
		for key, value in account.iteritems():
			if key in sensible_list:
				secure[key] = value
			else:
				plain[key] = value
		#print secure, plain

		if self.__use_gnome_keyring:
			id = self.__keyring_dict_helper.add_dict(secure, force_id)
			self.__file_dict_helper.add_dict(plain, force_id = id)
		else:
			id = self.__file_dict_helper.add_dict(account, force_id)
		return id
	
	def get_account_info_by_id(self, id):
                dicts = self.get_accounts_dicts()
                for account in dicts:
                        if account["id"] == id:
                                return account



if __name__ == "__main__":
	a = AccountManager()
	print a.get_accounts_dicts()
	
