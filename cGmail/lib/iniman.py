#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import ConfigParser
import os

class IniMan:
	
	def __init__(self, handler):
		self.__handler = handler
		
		self.__config = {}
		self.__load()
	
	def get_values(self):
		return self.__config
	
	def add(self, name, value):
		"""
		name must be in the form <section>.<option>
		"""
		self.__config[name] = value
		self.__store()
	
	def remove(self, name):
		"""
		name must be in the form <section>.<option>
		"""
		try:
			del self.__config[name]
			self.__store()
		except KeyError:
			pass
	
	def get_sections(self):
		sections = set([elem.split(".")[0] for elem in self.__config.keys()])
		return sections
	
	def get_by_section(self, section):
		ret = {}
		for elem in self.__config.keys():
			sec = elem.split(".")[0]
			if sec == section:
				ret[elem.split(".")[1]] = self.__config[elem]
		return ret

	def __load(self):
		"""
		returns a dictionary with key's of the form
		<section>.<option> and the values 
		"""
		self.__config = {}
		cp = ConfigParser.ConfigParser()
		cp.readfp(self.__handler)
		for sec in cp.sections():
			name = sec.lower()
			for opt in cp.options(sec):
				self.__config[name + "." + opt.lower()] = cp.get(sec, opt).strip()
	
	def __store(self):
		"""
		given a dictionary with key's of the form 'section.option: value'
		__store() generates a list of unique section names
		creates sections based that list
		use config.set to add entries to each section
		"""
		#print self.__config
		cp = ConfigParser.ConfigParser()
		sections = set([k.split('.')[0] for k in self.__config.keys()])
		map(cp.add_section, sections)
		for k,v in self.__config.items():
			s, o = k.split('.')
			cp.set(s, o, v)
		cp.write(self.__handler.get_new_writer())
		self.__handler.close()
	

if __name__ == "__main__":
	i = IniMan("test.ini")
	print i.get_values()
	i.add("azz.c", "beh")
	print i.get_values()
