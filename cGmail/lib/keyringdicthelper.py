#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


import gconf
import gnomekeyring
from ConfigParser import MissingSectionHeaderError

from dicthelper import DictHelper, CannotSaveError, CorruptedConfiguration
from storagehandler import StorageHandler

GCONF_AUTH_KEY = "/apps/cgmail/keyring_auth_token"

class KeyringHandler(StorageHandler):
	def __init__(self):
		self.__data = self.__read()
		self.__lines = self.__data.split('\n')
		self.__count = 0
		self.__to_write = ""

	def __read(self): 
		keyring = "default"
		secret = ""
		try:  
			gnomekeyring.create_sync(keyring, None)  
		except gnomekeyring.AlreadyExistsError:  
			pass 
	
		auth_token = gconf.client_get_default().get_int(GCONF_AUTH_KEY)
		if auth_token > 0:
			try:
				secret = gnomekeyring.item_get_info_sync(keyring, 
							auth_token).get_secret()
			except gnomekeyring.DeniedError:
				auth_token = 0
		return secret
	
	def readline(self):
		if self.__count < len(self.__lines):
			line = self.__lines[self.__count]
			self.__count += 1
			if not line:
				return "\n"
			return line
		else:
			return None
	
	def get_new_writer(self):
		self.__to_write = ""
		self.__store()
		return self

	def write(self, value):
		self.__to_write += value

	def __store(self):
		keyring = "default"
		try:
			gnomekeyring.create_sync(keyring, None)
		except gnomekeyring.AlreadyExistsError:
			pass
		
		try:
		   	auth_token = gnomekeyring.item_create_sync(
				keyring,
				gnomekeyring.ITEM_GENERIC_SECRET,
				"cgmail checker login informations",
				dict(appname="cgmail, gmail checker"),
				self.__to_write, True)
			gconf.client_get_default().set_int(GCONF_AUTH_KEY, auth_token)
		except:
			raise CannotSaveError()
	
	def close(self):
		self.__store()

	

class KeyringDictHelper(DictHelper):
	
	def __init__(self):
		keyring_handler = KeyringHandler()
		try:
			DictHelper.__init__(self, keyring_handler)
		except MissingSectionHeaderError:
			keyring_handler = keyring_handler.get_new_writer()
			raise CorruptedConfiguration

	

if __name__ == "__main__":
	import gtk
	k = KeyringDictHelper()
#	k.add_account({"username": "tesiot", "type": "gmail"})
#	k.remove_account(2)
	print k.get_dicts()
