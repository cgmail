#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import os
import sys

def load_checkers():
	"""
	Load all checkers modules and return a list
	"""
	path = os.path.abspath(os.path.dirname(__file__))
	checkers_path = os.path.join("..", "checkers")
	path = os.path.join(path, checkers_path)
	sys.path.append(path)
	modules = [x.split( '.' )[ 0 ] for x in os.listdir( path ) \
				if x.endswith( '.py' ) and not x.startswith( '_' ) \
				and x != 'base.py']
	__import__("checkers", globals(), locals(), [])
	checkers = []	
	for m in modules:
	#	print "Loading ", m
	        try:
        	        mod = __import__( "checkers" + '.' + m, globals(), locals(), [] )
                	checkers.append(getattr(mod, m))
	        except ImportError, e:
        	        print 'Exception importing ' + m + '\n' + str(e)
	return checkers

def get_checker_by_name(name):
	"""
	Return the checker named name
	"""
	checkers = load_checkers()
	for c in checkers:
		tmp = c.Info()
		if tmp.get_name() == name:
			return c
	return None

def get_checker_info_by_name(name):
	"""
	Return the Info object for the checker named name
	"""
	checker = get_checker_by_name(name)
	if checker is not None:
		return checker.Info()
	return None

