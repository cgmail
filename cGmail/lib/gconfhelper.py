#
#    Copyright (C) 2007 Marco Ferragina <marco.ferragina@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import gconf
import types
import gobject

class UnsupportedTypeException(Exception): pass

class GconfHelper:
	def __init__(self):
		self.basedir = "/apps/cgmail/"
		self.client = gconf.client_get_default()
		# We need this. If we don't use add_dir self.register_key_listner
		# will not work
		self.client.add_dir("/apps/cgmail", gconf.CLIENT_PRELOAD_NONE)
	
	def get_key(self, name):
		"""
		get a key
		"""
		path = self.basedir + name
		try:
			key = self.client.get_int(path)
		except gobject.GError:
			try:
				key = self.client.get_bool(path)
			except gobject.GError:
				try:
					key = self.client.get_string(path)
				except gobject.GError:
					try:
						key = self.client.get_float(path)
					except gobject.GError:
						raise UnsupportedTypeException()

		return key
	
	def set_key(self, name, value):
		"""
		Set a key detecting its type
		"""
		path = self.basedir + name
		if type(value) == types.IntType:
			self.client.set_int(path, value)
		elif type(value) == types.BooleanType:
			self.client.set_bool(path, value)
		elif type(value) == types.StringType:
			self.client.set_string(path, value)
		elif type(value) == types.FloatType:
			self.client.set_float(path, value)
		else:
			raise UnsupportedTypeException()
	
	def register_key_listner(self, key, cb):
		"""
		Register a listner on the key
		"""
		path = self.basedir + key
		self.client.notify_add(path, cb)


if __name__ == "__main__":
	import gtk
	g = GconfHelper()
	g.set_key("testint", 2)
	g.set_key("teststring", "value")
	g.set_key("testbool", True)
	g.set_key("testfloat", 3.123)
	print g.get_key("teststring")
	print g.get_key("testbool")
